package lesson_04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws FileNotFoundException{
//        testSwitchCase();
//        exceptionsTest();
//        System.out.println(fact(3));
//        outOfMemoryTest();
        exceptionsTest();
    }
    public static void testSwitchCase(){
        Scanner sc = new Scanner(System.in);
        int cnt = sc.nextInt();
        String s = sc.nextLine();
        if (cnt > 0) System.out.println("if clause");
        switch (s) {
            case "asd":
                System.out.println("switch clause 1");
                break;
            case "stop":
                System.out.println("switch clause 2");
                break;
        }
    }

    public static void exceptionsTest() {/*
        int[] arr = {1,2,3};
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " " + arr[i+1]);
        }*/
        File file = new File("asd.txt");
        Scanner sc; // sc = null
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        System.out.println(sc.nextLine());

    }
    public static int fact(int x){
        if (x == 1) return 1;
        return x * fact(x-1);
    }
    public static void outOfMemoryTest(){
        for (int i = 0; i < 100000; i++) {
            Object[] o = new Object[10000000];
        }
    }
}
