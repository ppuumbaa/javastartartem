package lesson_07.post_office;

public interface Sendable {
    boolean send();
}
