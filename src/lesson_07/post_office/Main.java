package lesson_07.post_office;

import lesson_07.post_office.Position;

public class Main {
    public static void main(String[] args) {
        PostOffice office = new ModernMail();
        MailOrder box = new Box(
                100, "Kazan", "Moscow",
                1231123,120383,false,
                3,3,3);

        MailOrder letter = new Letter(50,"MSC","KZN", 123132,
                123123, 3,4,"white");





        MailOrder[] orders  = {letter,box};
        int cnt = 0;
        for(MailOrder order: orders){
            if (office.sendOrder(order)) {
                cnt++;
            }
        }
        System.out.println("successfully sent orders: " + cnt);
    }

    public void send(Box mailOrder){
        mailOrder.send();
    }
}
