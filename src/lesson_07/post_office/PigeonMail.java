package lesson_07.post_office;

public class PigeonMail extends PostOffice{

    @Override
    public boolean sendOrder(Sendable order) {
        if (order instanceof Letter){
            System.out.println("sending...");
            return true;
        }else{
            System.out.println("unsupported order type");
            return false;
        }
    }
}
