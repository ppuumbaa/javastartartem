package lesson_07.post_office;

public enum Position {
    DIRECTOR(1000), ADMINISTRATOR(800), ORDINARY_WORKER(400), TRAINEE(200);

    private int salary;

    Position(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
