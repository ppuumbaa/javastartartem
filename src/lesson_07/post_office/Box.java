package lesson_07.post_office;


public class Box extends MailOrder {
    private boolean isFragile;
    private int height;
    private int width;
    private int length;

    public void packaging(){
        System.out.println("box packaging...");
    }

    public void unpacking(){
        System.out.println("box unpackaging...");
    }
    // public Box(...)
    public Box(int height, int width, int length){
        this.height = height;
        this.width = width;
        this.length = length;
    }







    public Box(int cost, String from, String to, int indexTo, int indexFrom, boolean isFragile, int height, int width, int length) {
        super(cost, from, to, indexTo, indexFrom);
        this.isFragile = isFragile;
        this.height = height;
        this.width = width;
        this.length = length;
    }

    public boolean isFragile() {
        return isFragile;
    }

    public void setFragile(boolean fragile) {
        isFragile = fragile;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public boolean send() {
        System.out.println("box sending...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            return false;
        }
        System.out.println("box successfully sent");
        return true;
    }

    @Override
    public void confirm() {
        System.out.println("Box received by addressee");
    }
}
