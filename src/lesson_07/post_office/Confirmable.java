package lesson_07.post_office;

public interface Confirmable {
    void confirm();
}
