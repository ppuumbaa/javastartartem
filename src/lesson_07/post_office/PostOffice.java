package lesson_07.post_office;

public abstract class PostOffice {
    protected String name;
    protected String street;
    protected String city;
    protected double rate;

    public PostOffice() {
    }

    public PostOffice(String street, String city, double rate) {
        this.street = street;
        this.city = city;
        this.rate = rate;
    }

    public void open(){
        System.out.println("office opened");
    }
    public void close(){
        System.out.println("office closed");
    }
    public abstract boolean sendOrder(Sendable order);
//    public abstract boolean sendOrder(Sendable[] order);

}
