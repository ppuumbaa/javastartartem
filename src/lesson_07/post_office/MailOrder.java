package lesson_07.post_office;

public abstract class MailOrder implements Sendable, Confirmable {
    protected int cost;
    protected String from;
    protected String to;
    protected int indexTo;
    protected int indexFrom;



    public MailOrder(){
    }

    public MailOrder(int cost, String from, String to, int indexTo, int indexFrom) {
        this.cost = cost;
        this.from = from;
        this.to = to;
        this.indexTo = indexTo;
        this.indexFrom = indexFrom;
    }
}
