package lesson_07.post_office;

public class Letter extends MailOrder {
    private int height;
    private int width;
    private String colorOfPaper;
    private String text;

    public void putInEnvelope(){
        System.out.println("putting...");
    }

    // todo: придумать еще один метод



    @Override
    public boolean send() {
        System.out.println("letter is sending...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            return false;
        }
        System.out.println("letter successfully sent");
        return true;
    }

    public Letter(){
        super();
    }

    public Letter(int cost, String from, String to, int indexTo, int indexFrom, int height, int width, String colorOfPaper) {
        super(cost, from, to, indexTo, indexFrom);
        this.height = height;
        this.width = width;
        this.colorOfPaper = colorOfPaper;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getColorOfPaper() {
        return colorOfPaper;
    }

    public void setColorOfPaper(String colorOfPaper) {
        this.colorOfPaper = colorOfPaper;
    }

    @Override
    public void confirm() {
        System.out.println("Letter received by addressee");
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
