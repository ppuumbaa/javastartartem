package lesson_07.post_office;

public class Pigeon implements Workable{
    private PigeonMail mail;
    private String color;
    private boolean isOld;

    public PigeonMail getMail() {
        return mail;
    }

    public void setMail(PigeonMail mail) {
        this.mail = mail;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isOld() {
        return isOld;
    }

    public void setOld(boolean old) {
        isOld = old;
    }

    public Pigeon(PigeonMail mail, String color, boolean isOld) {
        this.mail = mail;
        this.color = color;
        this.isOld = isOld;
    }

    @Override
    public void work() {
        System.out.println("Pigeon fly to work...");
    }
}
