package lesson_07.post_office;

public class ModernMail extends PostOffice{
    @Override
    public boolean sendOrder(Sendable order) {
        return order.send();
    }
}
