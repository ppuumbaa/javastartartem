package lesson_07.post_office;

import java.time.LocalDate;

public class Employee implements Workable{
    private LocalDate birthdate;
    private PostOffice office;
    private Position position;
    private String firstName;
    private String lastName;
    private int exp;


    public void getUniform(){
        System.out.println("getting uniform...");
    }

    public void changeSalary(int newSalary) {
        if (newSalary >= 200) {
            position.setSalary(newSalary);
        }else {
            System.out.println("This is a breach of contract!");
        }
    }


    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public Employee(LocalDate birthdate, String firstName) {
        this.birthdate = birthdate;
        this.firstName = firstName;
    }

    public Employee(LocalDate birthdate, String firstName, Position position, String lastName, int exp, PostOffice office) {
        this(birthdate,firstName);
        this.lastName = lastName;
        this.exp = exp;
        this.position = position;
        this.office = office;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public PostOffice getOffice() {
        return office;
    }


    public void setOffice(PostOffice office) {
        this.office = office;
    }

    @Override
    public void work() {
        System.out.println(firstName + " " + lastName +
                " from " + office.name + " is working....");
    }
}
