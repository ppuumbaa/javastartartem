package lesson_07;

public interface B {
    void a();
    default void b(){
        System.out.println("b from B");
    }
}
