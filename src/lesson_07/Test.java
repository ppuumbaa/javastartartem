package lesson_07;

public class Test implements A, B{
    @Override
    public void a() {
        System.out.println("It's working");
    }

    @Override
    public void b() {
        System.out.println("b from test");
    }


    public static void main(String[] args) {
        A var1 = new Test();
        B var2 = new Test();
        var1.a();
        var1.b();
        var2.a();
        var2.b();
    }
}
