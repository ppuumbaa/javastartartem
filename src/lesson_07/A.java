package lesson_07;

public interface A {
    void a();
    default void b(){
        System.out.println("b from A");
    }
}
