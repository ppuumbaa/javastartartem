package lesson_03;

import lesson_02.Lion;

public class Solution {
    public static void main(String[] args) {
        String s = asd(3);
        System.out.println(s);


/*
        int x = 3;
        String str = "blabla";
        asd(x, str);
        System.out.println(x +"  " +str );*/

/*        String s1 = asd(5);
        String s2 = asd();
        System.out.println(s1);
        System.out.println(s2);*/

//        System.out.println(Lion.type);
    }

    // функция - это именованный блок кода, возвращающий какое то значение
    // процедуры - это по сути под программа, блок кода который мы выделили для дальнейших вызовов. (void)
    public static String asd(int x) {
        x++;
        System.out.println("x = " + x);
        return "method with parameters";
    }

    // Перегрузка - это когда у нас есть несколько методов с одним названием но с разными параметрами
    // Сигнатура метода - это название метода и его параметры.
    public static String asd() {
        return "method without parameters";
    }



}

