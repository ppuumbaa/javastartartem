package lesson_03;

public class Homework {
    public static void main(String[] args) {
//        System.out.println("Результат 1ой задачи: ");
/*        byte maxValue = 127;
        System.out.println(maxValue);
        maxValue++;
        System.out.println(maxValue);*/
//        System.out.println(contains("qwerty","ery"));
        System.out.println(countUpperCase("asd asd asd"));
    }


    public static boolean contains(String source, String pattern) {
//        return false; // write your code here...Только toCharArray()
        /*
        * s: qwerty
        * p: xxx
        * */
        if (pattern.length() <= source.length()) {
            boolean isSubString;
            char[] sourceAsArray = source.toCharArray();
            char[] patternAsArray = pattern.toCharArray();
            for (int i = 0; i < sourceAsArray.length; i++) {
                if (patternAsArray[0] == sourceAsArray[i]) {
                    isSubString = true;
                    for (int j = 1; j < patternAsArray.length; j++) {
                        if (patternAsArray[j] != sourceAsArray[i + j]) {
                            isSubString = false;
                            break;
                        }
                    }
                    if (isSubString) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static int countUpperCase(String str){
//        return 0; // write your code here... (charAt())
        String[] split = str.split(" ");
        int cnt = 0;
        // char x = 'A';
        for (String s : split) {
            if (s.charAt(0) >= 'A' && s.charAt(0) <= 'Z') {
                cnt++;
            }
        }

        // тернарный оператор => (условие) ? a : b
//        System.out.println(cnt == 0 ? "Таких слов нет" : "Таких слов: " + cnt);
        if (cnt == 0){
            System.out.println("Таких слов нет");
        }else{
            System.out.println("Таких слов: " + cnt);
        }
        return cnt;
    }




}
