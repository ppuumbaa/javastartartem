package lesson_08;

public class Solution {
    public static void main(String[] args) {
        /* временная сложность  = O(n * n/2) = O(n * n) = O(n^2)
        * */
        int n = 5;
        int[] arr = {1,2,3,4,5}; // <- то что подали на вход (input)

        int[] storage = new int[n]; // <- создали дополнительно
        int[][] bigStorage = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println("hi");
                bigStorage[i][j] = arr[i];
            }

        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println(bigStorage[i][j]);

            }
        }

        // вр.сл. = O(n^2) + O(n) = max(O(n^2), O(n)) = O(n^2)
        // сложность по памяти = O(n)
    }


}
