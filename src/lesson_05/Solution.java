package lesson_05;

import java.util.Arrays;
import java.util.Objects;

public class Solution {
    public static void main(String[] args) {
        test1();

    }
    // Person (number, age, name, lastname, address, ...):
    // 4353123 - > 123
    // 4123124 - > 124


    public static void test1() {
        // [a, b, d, a, g]
        // 97 - a
        // 98 - b
        // 99 - c
        // 100 - d
        char[] a = new char[]{'a', 'c', 'e', 'f', 'g'};
        a[2] = 'd';
        char[] b = a;
        b[1] = 'b';
        b[3] = a[0];
        System.out.println(a[1] - a[2] + "" + a[3]);
    }

    /* 22 33 11 55
       11 99 12 44
       8  44 11 33
       13 78 77 13
    * */
    // 123 % 10 = 3
    // (123 / 10) % 10 = 2
    // 123 % 100 = 23
    // 123 / 10 = 12
    // while 123 > 0
    // услове
    // cnt = 2 - > 3

    //todo: протестировать
    public static boolean test2(int[][] arr) {
        for (int col = 0; col < arr[0].length; col++) {
            int cnt = 0;
            for (int[] ints : arr) {
                cnt = check(ints[col]) ? ++cnt : cnt;
            }
            if (cnt > 3) {
                return false;
            }
        }
        return true;
    }

    private static boolean check(int number) {
        if (number / 10 != 0) {
            while (number > 9) {
                int lastDigit = number % 10;
                int lastDigit2 = (number / 10) % 10;
                if (lastDigit == lastDigit2) {
                    number /= 10;
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
