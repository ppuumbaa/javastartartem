package lesson_01;

import java.util.Scanner;

public class StringTask {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        System.out.println(isPalindrom(s));



/*        StringBuilder newStr = new StringBuilder();
        for (int i = 0; i < 1000; i++) {
            newStr.append("asd");
        }*/

        Person p = new Person("Ivan", "Petrov", 18);
        System.out.println(p);
    }

    public static boolean isPalindrom(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isUnicString(String s) {
        boolean[] arr = new boolean[123];
        if (s.length() > 26) return false;
        for (int i = 0; i < s.length(); i++) {
            if (arr[s.charAt(i)]) {
                return false;
            } else {
                arr[s.charAt(i)] = true;
            }
        }
        return true;
    }


    static class Person{
        String name;
        String surname;
        int age;

        public Person(String name, String surname, int age){
            this.name = name;
            this.surname = surname;
            this.age = age;
        }

            @Override
            public String toString() {
                return "name: " + name + "\nsurname: " + surname + "\nage: " + age;
            }
    }
}
