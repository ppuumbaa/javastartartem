package lesson_01;

import lesson_02.Animal;
import lesson_03.Solution;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {


        Animal a = new Animal();
//        a.age = 2;
        int[][] arr = new int[3][];
        arr[0] = new int[]{3,5,2,5};
        arr[1] = new int[]{3,5,1,2,4,5};
        arr[2] = new int[]{3,4};

/*

        // Iterator ( hasNext(), next() ) используем любые классы реализующие интерфейс Iterable + массивы
        for (int[] ints : arr) {
            for (int anInt : ints) {
                anInt *= 2;
                System.out.print(anInt + " ");
            }
            System.out.println();
        }

        int[] a = {2,4,5,6};
        for (int i = 0; i < a.length; i++) {
            a[i] *= 2;
            System.out.print(a[i] + " "); // 4, 8...
        }
*/

/*        String s = "hello,world"; // -> HELLO
        String upperS = s.toUpperCase();
        String subString = s.substring(2); // llo
        String subString2 = s.substring(2,4); // ll
        String[] splitString = s.split(",");

        for (int i = 0; i < s.length(); i++) {
            System.out.println(s.charAt(i));
        }*/


/*        int x = 0;
        while (x < 10){
            System.out.println(x);
            x++;
        }

        do{
            x ++;
            System.out.println(x);
        }while (x < 10);*/

        String cat1 = "abc";
        String cat2 = "acd";
        System.out.println(cat1.compareTo(cat2));

        char x = (char) 97;
        System.out.println(x);
        System.out.println(cat1 == cat2); // true - потому что String pool, но как правило выводит false
        System.out.println(cat1.equals(cat2)); // true
    }
}
