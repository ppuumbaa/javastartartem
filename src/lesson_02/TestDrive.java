package lesson_02;

public class TestDrive {
    public static void main(String[] args) {
        Lion lion = new Lion(2,"orange");
        Lion lion2 = new Lion(1,"asdasd");
        System.out.println("before: " + lion);
//        lion.say();
//        lion.age = -2;
        lion.setAge(-2);


        System.out.println("after: " + lion);

        // тот тип который указан слева - указывает какие методы мы можем вызывать
        // тот тип который указан справа - указывает как именно эти методы будут себя вести
/*        Animal lion3 = new lesson_02.Lion(1,"asd");
        lion3.say();*/

        test(5);

    }

    public static void test(){}
    public static void test(int num){
        // code...
    }



    public static void testMethodSay(Animal[] animals){
        for(Animal animal: animals){
/*          как бы всё было без полиморфизма
            if (animal instanceof lesson_02.Lion ){
                lesson_02.Lion lion = (lesson_02.Lion) animal;
                lion.say();
            }

            if (animal instanceof Cat) {
                Cat cat = (Cat) animal;
                cat.say();
            }*/
            // а так - с полиморфизмом
            animal.say();
        }
    }
}
