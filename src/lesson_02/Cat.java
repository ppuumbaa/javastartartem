package lesson_02;

public class Cat extends Animal{

    public Cat(int age, String color) {
        super(age, color);
    }

    @Override
    public void say() {
        System.out.println("meow");
    }


}
