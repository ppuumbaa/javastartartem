package lesson_02;

import lesson_02.Animal;

public class Lion extends Animal {


    public static String type = "кошачьи";

    // 1. По умолчанию в каждом классе есть пустой конструктор (который пропадает при написании другого конструктора)
    // 2. В каждом конструкторе классов неявно первой строчкой вызывается конструктор родителя
    // 3. Когда мы пишем новые конструкторы - неявный пустой конструктор пропадает.

    public Lion(int age, String color) {
        super(age, color);
    }


    // this - это ссылка на объект нашего класса (lesson_02.Lion)
    // super - это ссылка на объект родительского класса (Animal)
    // Override - подстраховка себя же от ошибок при переопределении методов (например, опечатка в названии)
    // При переопределении методов мы можем увеличивать доступ, но не уменьшать
    @Override
    public void say() {
        System.out.println("rrrr");
    }



    public void bit(){
        System.out.println("lion bit someone");
    }

    @Override
    public String toString() {
        return "lesson_02.Lion{" +
                "age=" + super.getAge() +
                ", color='" + color + '\'' +
                '}';
    }
}
