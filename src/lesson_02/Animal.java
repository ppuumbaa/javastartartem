package lesson_02;

import java.util.Objects;

public class Animal {
    private int age;
    public  String color;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return age == animal.age && Objects.equals(color, animal.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, color);
    }

    // int x = animal.getAge();
    // animal.setAge(2);
    public int getAge(){
        return age;
    }

    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
        } else{
            System.out.println("Возраст не может быть < 0");
        }
    }


    protected void say(){ // издать голос
        System.out.println("animal say something");
    }


    public Animal(){}

    public Animal(int age, String color) {
        this.age = age;
        this.color = color;
    }
}
