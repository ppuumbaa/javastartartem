package lesson_06;


// Позднее / Раннее связывание
public class Cat extends Feline implements Predatory, Predatory2 {

    @Override
    public void makeNoise() {
        System.out.println("meow");
    }


    @Override
    public void bite() {
        System.out.println("кусь");
    }

    @Override
    public void blabla() {
        System.out.println("bla-bla");
    }
}
