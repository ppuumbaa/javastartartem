package lesson_06.shapes;

public class Test {
    public static void main(String[] args) {
        Shape[] shapes = {
                new Square(5),
                new Circle(3),
                new Rectangle(3,4)
        };
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].getSquare());
        }
    }
}
