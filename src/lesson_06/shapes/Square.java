package lesson_06.shapes;

public class Square extends Shape{

    private int side;

    public Square(int side){
        this.side = side;
    }

    @Override
    double getSquare() {
        return side*side;
    }

    public void setSide(int side) {
        this.side = side;
    }
}
