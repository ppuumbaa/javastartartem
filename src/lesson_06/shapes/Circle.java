package lesson_06.shapes;

public class Circle extends Shape {
    private double r;
    private final double PI = Math.PI;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    double getSquare() {
        return PI * r*r;
    }
}
