package lesson_06;

public abstract class Animal {
    private String country;

    public void sleep(){
        System.out.println("I'm sleeping...");
    }

    public abstract void makeNoise();

}
